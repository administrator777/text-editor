namespace TextEditorfin.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fin : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Models", "Name", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Models", "Name", c => c.String());
        }
    }
}
