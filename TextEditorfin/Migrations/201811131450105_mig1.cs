namespace TextEditorfin.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mig1 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Models");
            AddColumn("dbo.Models", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Models", "Name", c => c.String());
            AddPrimaryKey("dbo.Models", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.Models");
            AlterColumn("dbo.Models", "Name", c => c.String(nullable: false, maxLength: 128));
            DropColumn("dbo.Models", "Id");
            AddPrimaryKey("dbo.Models", "Name");
        }
    }
}
