﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace TextEditorfin.BD
{
     class DataBaseContext : System.Data.Entity.DbContext
     {
          public DataBaseContext() : base("name=cn") { }

          public virtual System.Data.Entity.DbSet<Model> Text { get; set; }
     }
}