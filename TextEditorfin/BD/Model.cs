﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TextEditorfin.BD
{
     class Model
     {
          [Key]
          public int Id { get; set; }

          [StringLength(255)]
          public string Name { get; set; }

          public string Text { get; set; }

          [NotMapped]
          public int ObjectState { get; set; }
     }
}
