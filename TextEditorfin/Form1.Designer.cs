﻿namespace TextEditorfin
{
     partial class MainWindow
     {
          /// <summary>
          /// Required designer variable.
          /// </summary>
          private System.ComponentModel.IContainer components = null;

          /// <summary>
          /// Clean up any resources being used.
          /// </summary>
          /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
          protected override void Dispose(bool disposing)
          {
               if (disposing && (components != null))
               {
                    components.Dispose();
               }
               base.Dispose(disposing);
          }

          #region Windows Form Designer generated code

          /// <summary>
          /// Required method for Designer support - do not modify
          /// the contents of this method with the code editor.
          /// </summary>
          private void InitializeComponent()
          {
               this.BtnCreate = new System.Windows.Forms.Button();
               this.BtnOpen = new System.Windows.Forms.Button();
               this.SuspendLayout();
               // 
               // BtnCreate
               // 
               this.BtnCreate.Location = new System.Drawing.Point(12, 12);
               this.BtnCreate.Name = "BtnCreate";
               this.BtnCreate.Size = new System.Drawing.Size(75, 23);
               this.BtnCreate.TabIndex = 0;
               this.BtnCreate.Text = "Create File";
               this.BtnCreate.UseVisualStyleBackColor = true;
               this.BtnCreate.Click += new System.EventHandler(this.BtnCreate_Click);
               // 
               // BtnOpen
               // 
               this.BtnOpen.Location = new System.Drawing.Point(127, 12);
               this.BtnOpen.Name = "BtnOpen";
               this.BtnOpen.Size = new System.Drawing.Size(75, 23);
               this.BtnOpen.TabIndex = 1;
               this.BtnOpen.Text = "Open File";
               this.BtnOpen.UseVisualStyleBackColor = true;
               this.BtnOpen.Click += new System.EventHandler(this.BtnOpen_Click);
               // 
               // MainWindow
               // 
               this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
               this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
               this.ClientSize = new System.Drawing.Size(214, 101);
               this.Controls.Add(this.BtnOpen);
               this.Controls.Add(this.BtnCreate);
               this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
               this.Name = "MainWindow";
               this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
               this.Text = "Main Window";
               this.ResumeLayout(false);

          }

          #endregion

          private System.Windows.Forms.Button BtnCreate;
          private System.Windows.Forms.Button BtnOpen;
     }
}

