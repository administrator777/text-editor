﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TextEditorfin
{
     public partial class MainWindow : Form
     {
          private EncryptDecrypt _security;
          TextEditor txtEdit = new TextEditor();
          PasswordWindow Passwind = new PasswordWindow();
          DataBase db = new DataBase();

          public MainWindow()
          {

               InitializeComponent();
               _security = new EncryptDecrypt();
          }

          private void BtnCreate_Click(object sender, EventArgs e)
          {
               try
               {
                    txtEdit.Show();
               }
               catch
               {
                    TextEditor txtEdit = new TextEditor();
                    txtEdit.Show();
               }
               return;
          }

          private void BtnOpen_Click(object sender, EventArgs e)
          {
               db.Show();
               //if (db.DialogResult == OK)
               OpenFileDialog theDialog = new OpenFileDialog
               {
                    Title = "Open Text File",
                    Filter = "txt files|*.txt",  
                    InitialDirectory = @"C:\"
               };
               if (theDialog.ShowDialog() == DialogResult.OK)
               {
                    Passwind.Show();
                    Passwind.Visible = false;
                    if (Passwind.ShowDialog() == DialogResult.OK)
                    {
                         string password = Passwind.Form2password;
                         TextEditor txtEdit = new TextEditor();
                         string s = File.ReadAllText(theDialog.FileName);
                         //decriptare 
                         string notEncryptedText = _security.Decrypt(password, s);
                         txtEdit.richTextBox1.Text = notEncryptedText;
                         txtEdit.Show();
                    }
                    else
                    {
                         return;
                    }
               }

               else
               {
                    return;
               }

          }
     }
}
