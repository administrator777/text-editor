﻿namespace TextEditorfin
{
     partial class TextEditor
     {
          /// <summary>
          /// Required designer variable.
          /// </summary>
          private System.ComponentModel.IContainer components = null;

          /// <summary>
          /// Clean up any resources being used.
          /// </summary>
          /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
          protected override void Dispose(bool disposing)
          {
               if (disposing && (components != null))
               {
                    components.Dispose();
               }
               base.Dispose(disposing);
          }

          #region Windows Form Designer generated code

          /// <summary>
          /// Required method for Designer support - do not modify
          /// the contents of this method with the code editor.
          /// </summary>
          private void InitializeComponent()
          {
               this.BtnSave = new System.Windows.Forms.Button();
               this.BtnDiscard = new System.Windows.Forms.Button();
               this.richTextBox1 = new System.Windows.Forms.RichTextBox();
               this.SuspendLayout();
               // 
               // BtnSave
               // 
               this.BtnSave.Location = new System.Drawing.Point(12, 12);
               this.BtnSave.Name = "BtnSave";
               this.BtnSave.Size = new System.Drawing.Size(75, 23);
               this.BtnSave.TabIndex = 0;
               this.BtnSave.Text = "Save";
               this.BtnSave.UseVisualStyleBackColor = true;
               this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
               // 
               // BtnDiscard
               // 
               this.BtnDiscard.Location = new System.Drawing.Point(93, 12);
               this.BtnDiscard.Name = "BtnDiscard";
               this.BtnDiscard.Size = new System.Drawing.Size(75, 23);
               this.BtnDiscard.TabIndex = 1;
               this.BtnDiscard.Text = "Discard";
               this.BtnDiscard.UseVisualStyleBackColor = true;
               // 
               // richTextBox1
               // 
               this.richTextBox1.Location = new System.Drawing.Point(12, 42);
               this.richTextBox1.Name = "richTextBox1";
               this.richTextBox1.Size = new System.Drawing.Size(838, 396);
               this.richTextBox1.TabIndex = 2;
               this.richTextBox1.Text = "";
               // 
               // TextEditor
               // 
               this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
               this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
               this.ClientSize = new System.Drawing.Size(862, 450);
               this.Controls.Add(this.richTextBox1);
               this.Controls.Add(this.BtnDiscard);
               this.Controls.Add(this.BtnSave);
               this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
               this.Name = "TextEditor";
               this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
               this.Text = "TextEditor";
               this.ResumeLayout(false);

          }

          #endregion

          private System.Windows.Forms.Button BtnSave;
          private System.Windows.Forms.Button BtnDiscard;
          public System.Windows.Forms.RichTextBox richTextBox1;
     }
}