﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TextEditorfin.BD;

namespace TextEditorfin
{
     public partial class DataBase : Form
     {
          DataBaseContext db = new DataBaseContext();

          public DataBase()
          {
               InitializeComponent();
          }

          private void DataBase_Load(object sender, EventArgs e)
          {
               //DataBase formdata = new DataBase();
               dataGridView1.DataSource = db.Text.ToList();
               using (db)
               {
                    modelBindingSource.DataSource = db.Text.ToList();
               }
          }

          private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
          {
               Model obj = modelBindingSource.Current as Model;
          }
     }
}
