﻿namespace TextEditorfin
{
     partial class PasswordWindow
     {
          /// <summary>
          /// Required designer variable.
          /// </summary>
          private System.ComponentModel.IContainer components = null;

          /// <summary>
          /// Clean up any resources being used.
          /// </summary>
          /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
          protected override void Dispose(bool disposing)
          {
               if (disposing && (components != null))
               {
                    components.Dispose();
               }
               base.Dispose(disposing);
          }

          #region Windows Form Designer generated code

          /// <summary>
          /// Required method for Designer support - do not modify
          /// the contents of this method with the code editor.
          /// </summary>
          private void InitializeComponent()
          {
               this.BtnPasswd = new System.Windows.Forms.Button();
               this.label1 = new System.Windows.Forms.Label();
               this.textBox1 = new System.Windows.Forms.TextBox();
               this.SuspendLayout();
               // 
               // BtnPasswd
               // 
               this.BtnPasswd.DialogResult = System.Windows.Forms.DialogResult.OK;
               this.BtnPasswd.Location = new System.Drawing.Point(265, 92);
               this.BtnPasswd.Name = "BtnPasswd";
               this.BtnPasswd.Size = new System.Drawing.Size(75, 23);
               this.BtnPasswd.TabIndex = 0;
               this.BtnPasswd.Text = "Login";
               this.BtnPasswd.UseVisualStyleBackColor = true;
               this.BtnPasswd.Click += new System.EventHandler(this.BtnPasswd_Click);
               // 
               // label1
               // 
               this.label1.AutoSize = true;
               this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
               this.label1.Location = new System.Drawing.Point(12, 47);
               this.label1.Name = "label1";
               this.label1.Size = new System.Drawing.Size(106, 25);
               this.label1.TabIndex = 1;
               this.label1.Text = "Password";
               // 
               // textBox1
               // 
               this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
               this.textBox1.Location = new System.Drawing.Point(124, 45);
               this.textBox1.Name = "textBox1";
               this.textBox1.PasswordChar = '*';
               this.textBox1.Size = new System.Drawing.Size(215, 29);
               this.textBox1.TabIndex = 2;
               // 
               // PasswordWindow
               // 
               this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
               this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
               this.ClientSize = new System.Drawing.Size(352, 142);
               this.Controls.Add(this.textBox1);
               this.Controls.Add(this.label1);
               this.Controls.Add(this.BtnPasswd);
               this.Name = "PasswordWindow";
               this.Text = "PasswordWindow";
               this.ResumeLayout(false);
               this.PerformLayout();

          }

          #endregion

          private System.Windows.Forms.Button BtnPasswd;
          private System.Windows.Forms.Label label1;
          public System.Windows.Forms.TextBox textBox1;
     }
}