﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TextEditorfin.BD;

namespace TextEditorfin
{
     public partial class TextEditor : Form
     {
          DataBase db = new DataBase();

          //SqlConnection myConnection = new SqlConnection();

          private EncryptDecrypt _security;
          PasswordWindow Passwind = new PasswordWindow();
          public TextEditor()
          {
               InitializeComponent();
               _security = new EncryptDecrypt();
          }

          private void BtnSave_Click(object sender, EventArgs e)
          {
               Passwind.ShowDialog();
               //*********************************

               //*********************************
               string password = Passwind.Form2password;
               richTextBox1.Text = _security.Encrypt(password, richTextBox1.Text);
               string s = richTextBox1.Text;
               // richTextBox1.SaveFile(saveFile1.FileName, RichTextBoxStreamType.PlainText);
               /*
               db.modelBindingSource.Add(new Model());
               db.modelBindingSource.MoveLast();
               richTextBox1.Focus();
               */
               using (var dbc = new DataBaseContext())
               {
                    Model obj = db.modelBindingSource.Current as Model;
                    if (obj != null)
                    {
                         if (dbc.Entry<Model>(obj).State == System.Data.Entity.EntityState.Detached)
                              dbc.Set<Model>().Attach(obj);

                         dbc.Entry<Model>(obj).State = System.Data.Entity.EntityState.Added;
                         dbc.Entry<Model>(obj).State = System.Data.Entity.EntityState.Modified;
                         dbc.SaveChanges();
                         db.Refresh();
                         obj.ObjectState = 0;
                    }
                    var text = new Model()
                    {
                         Text = s
                    };
                    dbc.Text.Add(text);
               }

               Close();
          }
          
          private void BtnDiscard_Click(object sender, EventArgs e)
          {
               Close();
          }
     }
}